({
    getTitles : function(component, helper) {
        let action = component.get('c.getOutOfStockTitles');

        action.setCallback(this, (response) => {
        	if(response.getState() === 'SUCCESS') {
		        // do stuff
		        let titles = response.getReturnValue();

		        console.log(titles);

		        component.set('v.titles', titles);
            }
        	else {
        	    console.error(response.getError());
            }
        });

        $A.enqueueAction(action);
    }
})