@isTest
private class RentalGeneratorTest {
    
    @isTest
    static void methodUnderTest_givenScenario_should() {
        
    }

    @isTest
    static void generateRentalForRentedStock_givenRentedStock_shouldCreateRental() {
        Title__c testTitle = new Title__c();
        testTitle.Name = 'Test Movie';
        testTitle.Genre__c = 'Action';
        insert testTitle;

        Stock__c testStock = new Stock__c();
        testStock.Title__c = testTitle.Id;
        testStock.Status__c = 'Available';
        insert testStock;

        Test.startTest();

        testStock.Status__c = 'Rented';
        update testStock;

        Test.stopTest();

        List<Rental__c> createdRentals = [SELECT Id FROM Rental__c];

        System.assertEquals(1, createdRentals.size(), 'A Rental should have been created.');
    }

    @isTest
    static void generateRentalForRentedStock_givenAlreadyRentedStock_shouldNotCreateAdditionalRentals() {
        Title__c testTitle = new Title__c();
        testTitle.Name = 'Test Movie';
        testTitle.Genre__c = 'Action';
        insert testTitle;

        Stock__c testStock = new Stock__c();
        testStock.Title__c = testTitle.Id;
        testStock.Status__c = 'Available';
        insert testStock;

        testStock.Status__c = 'Rented';
        update testStock;

        Test.startTest();

        update testStock;

        Test.stopTest();

        List<Rental__c> createdRentals = [SELECT Id FROM Rental__c];

        System.assertEquals(1, createdRentals.size(), 'Only one Rental should have been created.');
    }

    @isTest
    static void generateRentalForRentedStock_givenNonRentedUpdatedStock_shouldNotCreateRental() {
        Title__c testTitle = new Title__c();
        testTitle.Name = 'Test Movie';
        testTitle.Genre__c = 'Action';
        insert testTitle;

        Stock__c testStock = new Stock__c();
        testStock.Title__c = testTitle.Id;
        testStock.Status__c = 'Available';
        insert testStock;

        Test.startTest();

        testStock.Status__c = 'Damaged';
        update testStock;

        Test.stopTest();

        List<Rental__c> createdRentals = [SELECT Id FROM Rental__c];

        System.assertEquals(0, createdRentals.size(), 'No Rentals should have been created.');
    }

}