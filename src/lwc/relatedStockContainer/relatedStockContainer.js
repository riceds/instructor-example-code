import { LightningElement, wire } from 'lwc';
import getRelatedStock from '@salesforce/apex/StockAuraService.getRelatedStock';

import {subscribe, MessageContext } from 'lightning/messageService';
import SELECTED_TITLE_MC from '@salesforce/messageChannel/Selected_Title__c';

export default class RelatedStockContainer extends LightningElement {

    title;
    stocks;
    selectedStockId;

    @wire(MessageContext)
    messageContext;

    connectedCallback(){
        subscribe(this.messageContext, SELECTED_TITLE_MC, (message) => this.handleMessage(message));
    }

    handleStockClick(event) {
        this.selectedStockId = event.detail;
    }

    handleMessage(message){
        console.log('message received');
        this.title = message.title;
        let titleId = this.title.Id;
        getRelatedStock({titleId: titleId}).then(response =>{
            this.stocks = response;
            console.log(response);
        }).catch(error => {
            console.log(error);
        })
    }

}